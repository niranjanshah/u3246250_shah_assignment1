# Assignment 1
### Author: Niranjan Shah
### StudentID: u3246250

## Description
The files in the repo are described as below:
1. `Part_a.ipynb` contains the code related to EDA
2. `Part_b.ipynb` file contains code related to data cleaning and modelling
3. `create_data_for_tableau.ipynb` doesnt contain data related to assignment. But it contains code to transform the original data so that it can be used in the tableau dashboard.
4. `Dockerfile` contains configs related to how to configure docker container
5. `u3246250_shah_assignment1.twb` contains the tableau dashboard
6. `requirements.txt` contains the libraries and their version required

## Steps to run it locally
* Install all the required libraries mentioned in requirements.txt
* Run the notebook `Part_a.ipynb` in jupyter notebook to see the EDA
* Run the notebook `Part_b.ipynb` in jupyter notebook line by line to see the cleaning process, train the model and show evaluations regarding it.
* The dashboard can be viewed using `u3246250_shah_assignment1.twb` or from https://public.tableau.com/app/profile/niranjan.shah.shah/viz/u3246250_shah_assignment1/Dashboard1

## Steps to run it using docker
The docker image has been uploaded to dockerhub. You can download the image, and run it locally without installing all the libraries one by one or setting up the env
The steps to run using docker are below:
* Pull the docker image using `docker pull noobieblank/ds_assignment_1:latest`
* Run the command to run the docker container `docker run -p 8000:8888 noobieblank/ds_assignment_1 start-notebook.sh --NotebookApp.token=''`
* Goto `localhost:8000` on any browser. It will open jupyter notebook, where you can open the notebook files to run the code.