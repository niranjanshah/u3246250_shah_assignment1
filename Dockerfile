FROM jupyter/base-notebook:python-3.11
# Set the working directory inside the container
WORKDIR /usr/src/app

# saving copying the files
COPY . .

# installing dependencies
RUN pip install --no-cache-dir -r requirements.txt

# running jupyter command to run the notebook
CMD ["jupyter", "notebook", "--ip='*'", "--port=8888", "--no-browser", "--allow-root"]